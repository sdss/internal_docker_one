default: internal_one

internal-docker-dev: apache admin internal

apache:
	docker build -t webserver webserver/apache/.

admin:
	docker build -t users users/admin/.

internal:
	docker build -t host host/internal/.

internal_one:
	docker build -t internal_one .
