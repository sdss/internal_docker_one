build
-----
    make

is equivalent to:

    make internal-docker
    docker build -t internal_one .

run at nersc
------------
    docker run -d -p 128.55.210.142:5022:22 \
               -p 128.55.210.142:80:80 \
               -p 128.55.210.142:443:443 \
               -p 128.55.210.142:873:873  \
               -v /global/u2/j/joel/internal.sdss.org:/home/sdssadmin/internal.sdss.org:rw \
               -h internal01 -e HOST_DOMAIN="internal.mirror.sdss.org" --name internal01 sdss4/internal_docker_one
    

run on dhn01.sdss.utah.edu
--------------------------
docker run -d -p 155.101.19.47:5022:22 -p 155.101.19.47:5080:80 -p 155.101.19.47:5873:5873 -v /home/sdssadmin/internal.sdss.org:/home/sdssadmin/internal.sdss.org:rw -h internal01 -e HOST_DOMAIN="dhn01.sdss.utah.edu" --name internal01 internal_one

docker run -d -p 155.101.19.47:5022:22 -p 155.101.19.47:5080:80 -p 155.101.19.47:5873:5873 -v /home/sdssadmin/internal.sdss.org:/home/sdssadmin/internal.sdss.org:rw -h internal04 -e HOST_DOMAIN="dhn01.sdss.utah.edu" --name internal04 sdss4/internal_docker_one


run on desktop
--------------------------
    docker run -d -p 5022:22 -p 5080:80 -p 5873:5873 -v /Users/joel/Desktop/internal_docker_one/internal.sdss.org:/home/sdssadmin/internal.sdss.org:rw -h internal01 -e HOST_DOMAIN="192.168.99.100" --name internal01 internal_one

inspect image label
-------------------
$  docker inspect -f "{{json .ContainerConfig.Labels }}" internal_one
{"build":"HTTPD 2.4 with subversion (April 14 2015 14:15:00MDT)"}