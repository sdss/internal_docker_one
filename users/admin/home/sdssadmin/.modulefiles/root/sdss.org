#%Module1.0
proc ModulesHelp { } {
    global product version
    puts stderr "This module adds $product/$version to your environment."
}
#
# Define variables
#
set product root
set version sdss.org
set host internal
#
# module-whatis
#
module-whatis "Sets up $product/$version in your environment."
#
# Set environment
set WWW_DIR                             /home/www
set DIR                                 /home/sdssadmin
setenv [string toupper $product]_URL    $version
setenv [string toupper $host]_WWW_DIR   $WWW_DIR/$host.$version
setenv [string toupper $host]_DIR       $DIR/$host.$version
#
# Set aliases
#
set-alias apache "sudo service apache2"

