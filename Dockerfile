# sdss/internal_docker

FROM ubuntu:15.10
MAINTAINER Joel Brownstein <joelbrownstein@astro.utah.edu>
LABEL build="Ubuntu 15.10 with apache subversion (April 14 2015 14:15:00MDT)"

# ========================================================
# WEBSERVER> apache
# ========================================================

RUN apt-get update \
    && apt-get -y install apache2 subversion libapache2-svn \
    && apt-get clean

ADD webserver/apache/etc/apache2/ etc/apache2/

RUN chmod -R o+rX etc/apache2 \
    && /usr/sbin/a2dissite 000-default \
    && /usr/sbin/a2ensite default-ssl \
    && /usr/sbin/a2ensite internal.conf \
    && /usr/sbin/a2enmod ssl \
    && /usr/sbin/a2enmod auth_digest \
    && /usr/sbin/a2enmod dav_svn \
    && /usr/sbin/a2enmod authz_svn

# ========================================================
# USERS > admin
# ========================================================

RUN apt-get update \
    && apt-get install -y vim sudo rsyslog lmod tclsh openssh-server \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/bin
ADD users/admin/tmp/bin /tmp/bin/
RUN chmod +x /tmp/bin/*
RUN mkdir -p /var/run/sshd && sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config \
    && sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed -ri 's/^session\s+required\s+pam_loginuid.so$/session optional pam_loginuid.so/' /etc/pam.d/cron \
    && sed -ri 's/^session\s+required\s+pam_loginuid.so$/session optional pam_loginuid.so/' /etc/pam.d/sshd \
    && ln -s /usr/share/lmod/lmod/init/env_modules_python.py /usr/share/lmod/lmod/init/python \
    && touch /root/.Xauthority \
    && true

# Add Users and Groups
RUN groupadd sdss
RUN useradd -g sdss sdssadmin \
    && chsh -s /bin/bash sdssadmin \
    && passwd -d sdssadmin \
    && mkdir /home/sdssadmin \
    && addgroup sdssadmin sdss \
    && addgroup sdssadmin staff \
    && true

ADD users/admin/etc/motd /etc/motd
ADD users/admin/home/sdssadmin /home/sdssadmin/
RUN chmod -x /etc/update-motd.d/* \
    && chmod u+x,g+x,o+x /home/sdssadmin/crontab/svn/* \
    && chown -R sdssadmin:sdss /home/sdssadmin \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

ENV SSH_KEY ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHQ/qcYdnnlKUy295NZQBrq3kiv4u34W+8AEPRe35/RppAy/wYmaTMK0oE7NXFnatxA+7sFz8BOfHI4si5MHt/wn8tgB8XteSMmYFgLi9BExMlx+7q9jt6tMzUy7gYdx6sh9KCS5hoH8HSt2E5MoZK57UqUMMMaBfcWvBYyt0y+WXujTN1AliAgnI3VdywZP8/k4UQ4K+FwTYSOt3bSzmtD/YdD8ngk+2DSpjASl/1Z16ESz6jYJxqvBhdgT81Q6+Bp8/K8NQrXfKJHNWW1P3yTe5m+DI7LvDu8Bae3fSMvuq4/Vce0Et1HTEvVebCinPO0xmrg3j9LvFtqvVjZjvl joel@dhn

# ========================================================
# HOST > internal.sdss.org
# ========================================================

ADD host/internal/home/www/ /home/www/
RUN chown -R sdssadmin:sdss /home/www/internal.sdss.org \
    && chown sdssadmin:sdss /tmp/bin/svnadmin* \
    && rm -fr /var/log/apache2 \
    && ln -sf /home/sdssadmin/internal.sdss.org/log/apache2 /var/log/ \
    && locale-gen en_US.UTF-8

EXPOSE 22 80 443

CMD ["/tmp/bin/start_services"]

